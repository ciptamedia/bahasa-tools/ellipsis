var re = new RegExp('\\b\\.\\.\\.\\B', 'g');

module.exports = function (text, opts) {
  var suggestions = [];
  while (match = re.exec(text)) {
      suggestions.push({
        index: match.index,
        offset: 3,
      });
  }
  return suggestions;
};
