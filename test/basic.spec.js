var passive = require('../ellipsis');

describe('ellipsis', function () {

  it('should detect ellipsis', function () {
    expect(ellipsis('He was judged...')).toEqual([{ index: 3, offset: 3 }]);
  });

});
